﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using CarsManager.Models;

namespace CarsManager.Data
{
    public static class DbInitializer
    {
        public static void Initialize(CarsContext context)
        {
            //context.Database.EnsureCreated();

            // Look for any students.
            if (context.Cars.Any())
            {
                return;   // DB has been seeded
            }

            var cars = new Car[]
            {
                new Car { Manufacture="BMW", Model = "Z5", Engine=3000, ProductionDate = DateTime.Parse("2012-09-01") },
                new Car { Manufacture="BMW", Model = "Z7", Engine=3500, ProductionDate = DateTime.Parse("2015-09-01") },
                new Car { Manufacture="BMW", Model = "m3", Engine=2500, ProductionDate = DateTime.Parse("2017-09-01") },
            };

            foreach (Car cc in cars)
            {
                context.Cars.Add(cc);
            }
            context.SaveChanges();

        }
    }
}